<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['name' , 'publisher', 'price', 'year', 'image'];
}
