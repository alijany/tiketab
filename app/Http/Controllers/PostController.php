<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function create(Request $request)
    {

    }


    public function store(Request $request)
    {
        $post = new Post();

        $post->user_id = auth()->user()->id;
        $post->title = $request->title;
        $post->body = $request->body;

        if($file = $request->hasFile('post_image')) {

            $file = $request->file('post_image') ;

            $fileName = $file->getClientOriginalName() ;
            $destinationPath = public_path().'/images/' ;
            $file->move($destinationPath,$fileName);
            $post->image_name = $fileName ;
        }

        $post->save();

    }


    public function remove( Request $request)
    {
        $post = Post::find($request->id);
        $post->delete();
    }
}
