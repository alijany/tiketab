<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class BookController extends Controller
{
    public function store(Request $request)
    {
        $book = new Book();

        $book->name = $request->name;
        $book->publisher = $request->publisher;
        $book->price = $request->price;
        $book->year = $request->year;

        if($file = $request->hasFile('book_image')) {

            $file = $request->file('book_image') ;

            $fileName = $file->getClientOriginalName() ;
            $destinationPath = public_path().'/images/' ;
            $file->move($destinationPath,$fileName);
            $book->image = $fileName ;
        }

        $book->save();

        return redirect()->route('home');
    }



    public function remove( Request $request)
    {
        $book = Book::find($request->id);
        $book->delete();
    }


    public function all()
    {
        $books = Book::all();
        return view('index' , ['books' => $books]);
    }

    public function showcreate()
    {
        return view('bookcreate');
    }

}






