<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::post('/post', 'HomeController@save')->name('save');
Route::delete('/post', 'HomeController@remove')->name('remove');

Route::get('/createbook', 'BookController@showcreate');
Route::post('/createbook', 'BookController@store');

Route::view('/user', 'user')
    ->middleware('auth');

Route::get('/books', function () {
    return view('books', ['books' => App\Book::all()]);
});

Route::get('/book/{id}', function ($id) {
    return view('book', ['book' => App\Book::find($id)]);
});

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
