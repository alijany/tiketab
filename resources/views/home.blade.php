@extends('layouts.app')

@section('content')
<div class="container" dir=""rtl>
    <div class="row">
        <div class="col-lg-2">
            <ul class="ul-in-home">
                <li class="text-right">
                    <img src="images/home.png">
                    <span class="mr-2">خانه</span>
                </li>
                <li class="text-right">
                    <img src="images/home.png">
                    <span class="mr-2">دسته بندی</span>
                </li>
                <li class="text-right">
                    <img src="images/home.png">
                    <span class="mr-2">آخرین کتاب ها</span>
                </li>
                <li class="text-right">
                    <img src="images/home.png">
                    <span class="mr-2">محبوب ترین ها</span>
                </li>
                <li class="text-right">
                    <img src="images/home.png">
                    <span class="mr-2">درباره ما</span>
                </li>
            </ul>
        </div>

        <div class="col-lg-10 ">
            <div class="row">
                <div class="col-lg-6 col-md-12 text-center justify-content-center">
                    <img src="images/sliderImg.png" style="height: 300px">
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="row justify-content-center mb-auto">
                        <img src="images/book1.png" style="border-radius: 10px; height: 140px">
                    </div>
                    <div class="row justify-content-center mt-auto">
                        <img src="images/book2.png" style="border-radius: 10px; margin-top: 20px; height: 140px">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
