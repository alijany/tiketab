<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Example page</title>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

</head>

<body>
    
    {{-- <img class="w-100" src="{{ URL::asset('assets/gift-lg.png') }}" alt="gift"> --}}

    <script src="{{ mix('/js/app.js') }}"></script>
</body>

</html>