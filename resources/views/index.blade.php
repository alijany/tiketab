<!DOCTYPE html>
<html lang="fa-IR" dir="rtl">

<head>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,700,700italic,900,900italic"
        rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/js/fontawesome.min.js"
        integrity="sha256-bP9MBDJ4xkv81w/A2cgwRonMI+eelvZwm7e8rP5JIxA=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href={{ mix("/css/index.css")}}>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css"
        integrity="sha256-46r060N2LrChLLb5zowXQ72/iKKNiw/lAmygmHExk/o=" crossorigin="anonymous" />

    <link rel="stylesheet" href="{{ mix('/css/index.css') }}">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <title>Template</title>

    <style>
        html {
            scroll-behavior: smooth;
        }

        .fixed {
            background-color: unset;
            text-align: right;
        }
    </style>
</head>


<body>
    <!-- Login-Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>ورود/عضویت</h5>
                </div>
                <div class="modal-body">
                    <input type='checkbox' id='form-switch'>
                    <form id='login-form' action="" method='post'>
                        <div class="login-div">
                            <input class="login-input" type="text" placeholder="نام کاربری" required>
                            <input class="login-input" type="password" placeholder="کلمه ی عبور" required>
                        </div>
                        <div class="button-div">
                            <button class="login-button" type='submit'>ورود</button>
                            <label for='form-switch'><span>ثبت‌نام در تی کتاب</span></label>
                        </div>
                    </form>
                    <form id='register-form' action="" method='post'>
                        <div class="signup-div">
                            <input class="login-input" type="text" placeholder="نام کاربری" required>
                            <input class="login-input" type="email" placeholder="پست الکترونیکی" required>
                            <input class="login-input" type="password" placeholder="کلمه ی عبور" required>
                        </div>
                        <div class="button-div">
                            <button class="login-button" type='submit'>ثبت نام</button>
                            <label class="switch-label" for='form-switch'>وارد شویــد</label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="grid-container">

        <!-- Head For Responsive Mode -->
        <div class="HResponsive">
            <div class="flex-container1">
                <div>
                    <a href="#">
                        <img alt="TiKetab-Logo" src="{{ URL::asset('assets/logo.png') }}"
                            style="top: 50%; left: 15%; position: relative;" width="90px" />
                    </a>
                </div>
                <div>
                    <a href="#">
                        <ion-icon name="grid-outline"
                            style="position: relative; top: 59%; right:130%; font-size: 35px;color: #999999;">
                        </ion-icon>
                    </a>
                </div>
            </div>

            <header style="height: 93px;">
                <div class="flex-container2">
                    <div style="position: relative; top: 77%;">
                        <a id="signin" href="#" type="button" data-toggle="modal" data-target="#loginModal">
                            <i class="far fa-user" style="
                        border: 1px solid;
                        padding: 10px;
                        border-radius: 100%;
                        font-size: x-small;
                      "></i> ورود / عضویت
                        </a>
                    </div>
                    <div style="position: relative; top: 77%;">
                        <a href={{url("/createbook")}}>
                            <i class="fas fa-plus" style="
                        padding: 10px;
                        border: 1px solid #999999;
                        border-radius: 100%;
                        font-size: x-small;
                      "></i> ثبت کتاب
                        </a>
                    </div>
                    <div style="position: relative; top: 77%;">
                        <a href="#">
                            <i class="far fa-bell" style="
                        border: 1px solid #999999;
                        padding: 10px;
                        border-radius: 100%;
                        font-size: x-small;
                      "></i> اعلان ها
                        </a>
                    </div>
                </div>
            </header>
            <hr style="color: #f7f7f7; margin-bottom: 2rem;" />
        </div>


        <!-- Head For Normal Mode -->
        <header class="header">
            <div class="fixed">
                <div class="flex-container">
                    <div class="menuleft">


                        @auth
                        <span>
                            <a href="user">
                                <i class="far fa-user"
                                    style=" border: 1px solid;padding: 10px;border-radius: 100%;"></i>
                                {{Auth::user()->name}}
                            </a>
                            <a href="{{ url('logout') }}">
                                | خروج |
                            </a>
                        </span>
                        @endauth

                        @guest
                        <a href="login">
                            <i class="far fa-user"
                                style=" border: 1px solid;padding: 10px;border-radius: 100%;"></i>عضویت/ورود
                        </a>
                        @endguest

                        <a href="#">
                            <img src="{{ URL::asset('assets/logo.png') }}" alt="TiKetab-Logo" width="100px">
                        </a>
                    </div>
                    <div class="menuright">
                        <a href="{{url("/createbook")}}">
                            <i class="far fa-plus-square"></i> ثبت کتاب
                        </a>
                        <a href="#">
                            <i class="far fa-bell"></i> اعلان ها
                        </a>
                    </div>
                </div>
            </div>
        </header>

        <aside class="aside1">
            <div class="fixed">
                <ul>
                    <li>
                        <a href="#" class="active">
                            <i class="fas fa-home"></i>خانه
                        </a>
                    </li>


                    <li>
                        <a href="#latest-books">
                            <i class="fas fa-redo"></i>آخرین کتاب ها
                        </a>
                    </li>

                    <li>
                        <a href="#cat">
                            <i class="fas fa-boxes"></i>دسته بندی
                        </a>
                    </li>

                    <li>
                        <a href="#fav-book">
                            <i class="far fa-heart"></i>محبوب ترین کتاب ها
                        </a>
                    </li>

                    <li>
                        <a href="#footer">
                            <i class="fas fa-info"></i>درباره ما
                        </a>
                    </li>


                </ul>

                <a href="#" class="support">
                    <i class="fas fa-headset"></i>پشتیبانی
                </a>
            </div>
        </aside>


        <div class="main">
            <div class="intro">

                {{-- carousel --}}
                <div id="my-carousel" class="carousel slide item">
                    <div class="carousel-inner">
                        <div class="carousel-item active container-fluid">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#my-carousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#my-carousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div>

                <div class="item">
                    <img src="{{ URL::asset('assets/untitled.jpg') }}" alt="">
                    <p style="font-weight: 800;">کتاب برای فروش داری؟</p>
                    <a href={{url("/createbook")}}>اینجا کلیک کن</a>
                </div>
                <div class="item">
                    <img src="{{ URL::asset('assets/untitled3.jpg') }}" alt="">
                    <p style="font-weight: 800;">دنبال کتاب میگردی؟</p>
                    <a href="books">اینجا کلیک کن</a>
                </div>
            </div>

            <div id="latest-books">
                <div class="title">
                    <h4>آخرین کتاب ها</h4>
                    <p>بیشتر ...</p>
                </div>
                <div class="book_slideshow">
                    <i class="fas fa-chevron-left left"></i>
                    <i class="fas fa-chevron-right right"></i>

                    @foreach($books as $book)

                    @if($book->id == 1)
                    <div class="book">
                        <div class="book_Images">
                            <div class="bimg">
                                <img src="Images/{{$book->image}}" alt="">
                            </div>
                        </div>
                        <div class="book_Details">
                            <p>{{$book->name}}</p>
                            <p>{{$book->publisher}}</p>
                            <p>{{$book->price}}</p>
                        </div>
                        <button><a href="book/{{$book->id}}">مشاهده</a></button>
                        <i class="fas fa-heart like" style="position: absolute; left:2%"></i>
                    </div>
                    @endif


                    @if($book->id == 2)
                    <div class="book">
                        <div class="book_Images">
                            <div class="bimg">
                                <img src="Images/{{$book->image}}" alt="">
                            </div>
                        </div>
                        <div class="book_Details">
                            <p>{{$book->name}}</p>
                            <p>{{$book->publisher}}</p>
                            <p>{{$book->price}}</p>
                        </div>
                        <button><a href="book/{{$book->id}}">مشاهده</a></button>
                        <i class="fas fa-heart like" style="position: absolute; left:2%"></i>
                    </div>
                    @endif

                    @if($book->id == 3)
                    <div class="book">
                        <div class="book_Images">
                            <div class="bimg">
                                <img src="Images/{{$book->image}}" alt="">
                            </div>
                        </div>
                        <div class="book_Details">
                            <p>{{$book->name}}</p>
                            <p>{{$book->publisher}}</p>
                            <p>{{$book->price}}</p>
                        </div>
                        <button><a href="book/{{$book->id}}">مشاهده</a></button>
                        <i class="fas fa-heart like" style="position: absolute; left:2%"></i>
                    </div>
                    @endif


                    @endforeach

                </div>
            </div>
            <section class="cat" id="cat">
                <div class="txt">
                    <p>دسته بندی</p>
                    <p>مشاهده همه...</p>
                </div>
                <div class="keyword">
                    <div class="key">لورم</div>
                    <div class="key">لورم ایپسون</div>
                    <div class="key">لورم ایپسون</div>
                    <div class="key">لورم ایپسون</div>
                    <div class="key">لورم ایپسون</div>
                    <div class="key">لورم ایپسون</div>
                    <div class="key">لورم ایپسون</div>
                    <div class="key">لورم ایپسون</div>
                    <div class="key">متن ساختگی</div>
                    <div class="key">متن</div>
                    <div class="key">نوشته</div>
                    <div class="key">نوشته</div>
                    <div class="key">متن ساختگی</div>
                    <div class="key">نوشته</div>
                    <div class="key">نوشته</div>
                    <div class="key">نوشته</div>
                    <div class="key">نوشته</div>
                    <div class="key">نوشته</div>
                    <div class="key">لورم</div>
                    <div class="key">لورم</div>
                    <div class="key">متن ساختگی</div>
                    <div class="key">لورم ایپسون</div>
                </div>
                <img src="{{ URL::asset('assets/untitled5.png') }}" alt="">
            </section>




            <div class="last_books" id="fav-book">
                <div class="title">
                    <h4>محبوب ترین ها</h4>
                    <span>بیشتر ...</span>
                </div>
                <div class="book_slideshow">
                    <i class="fas fa-chevron-left left"></i>
                    <i class="fas fa-chevron-right right"></i>
                    @foreach($books as $book)

                    @if($book->id ==1)
                    <div class="book">
                        <div class="book_Images">
                            <div class="bimg">
                                <img src="Images/{{$book->image}}" alt="">
                            </div>
                        </div>
                        <div class="book_Details">
                            <p>{{$book->name}}</p>
                            <p>{{$book->publisher}}</p>
                            <p>{{$book->price}}</p>
                        </div>
                        <button><a href="book/{{$book->id}}">مشاهده</a></button>
                        <i class="fas fa-heart like"></i>
                    </div>
                    @endif

                    @if($book->id ==2)
                    <div class="book">
                        <div class="book_Images">
                            <div class="bimg">
                                <img src="Images/{{$book->image}}" alt="">
                            </div>
                        </div>
                        <div class="book_Details">
                            <p>{{$book->name}}</p>
                            <p>{{$book->publisher}}</p>
                            <p>{{$book->price}}</p>
                        </div>
                        <button><a href="book/{{$book->id}}">مشاهده</a></button>
                        <i class="fas fa-heart like"></i>
                    </div>
                    @endif

                    @if($book->id ==3)
                    <div class="book">
                        <div class="book_Images">
                            <div class="bimg">
                                <img src="Images/{{$book->image}}" alt="">
                            </div>
                        </div>
                        <div class="book_Details">
                            <p>{{$book->name}}</p>
                            <p>{{$book->publisher}}</p>
                            <p>{{$book->price}}</p>
                        </div>
                        <button><a href="book/{{$book->id}}">مشاهده</a></button>
                        <i class="fas fa-heart like"></i>
                    </div>
                    @endif

                    @endforeach

                </div>
            </div>
        </div>
        <footer class="footer" id="footer">
            <div class="footer_item">
                <h3>درباره ما</h3>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها
                    و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و
                    کاربردهای متنوع با هدف بهبود ابزارهای
                    کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را
                    می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ
                    پیشرو در زبان فارسی ایجاد کرد.</p>
            </div>
            <div class="footer_item" id="a">
                <img src="{{ URL::asset('assets/untitled4.jpg') }}" id="b">
                <img src="{{ URL::asset('assets/lg.png') }}" alt="" id="c">
                <a href="#"><i id="d" class="far fa-comment"></i></a>
                <a href="#"><i id="e" class="fas fa-envelope"></i></a>
                <a href="#"><i id="f" class="fab fa-instagram"></i></a>
            </div>
        </footer>

    </div>

    <script src="{{ mix('/js/app.js') }}"></script>
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js">
        </body>

</html>