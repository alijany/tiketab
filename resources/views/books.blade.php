<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,700,700italic,900,900italic"
        rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/js/fontawesome.min.js"
        integrity="sha256-bP9MBDJ4xkv81w/A2cgwRonMI+eelvZwm7e8rP5JIxA=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css"
        integrity="sha256-46r060N2LrChLLb5zowXQ72/iKKNiw/lAmygmHExk/o=" crossorigin="anonymous" />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">

    <link rel="stylesheet" href="{{ mix('/css/index.css') }}">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

    <title>Document</title>
</head>

<body>

    <!-- Login-Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>ورود/عضویت</h5>
                </div>
                <div class="modal-body">
                    <input type='checkbox' id='form-switch'>
                    <form id='login-form' action="" method='post'>
                        <div class="login-div">
                            <input class="login-input" type="text" placeholder="نام کاربری" required>
                            <input class="login-input" type="password" placeholder="کلمه ی عبور" required>
                        </div>
                        <div class="button-div">
                            <button class="login-button" type='submit'>ورود</button>
                            <label for='form-switch'><span>ثبت‌نام در تی کتاب</span></label>
                        </div>
                    </form>
                    <form id='register-form' action="" method='post'>
                        <div class="signup-div">
                            <input class="login-input" type="text" placeholder="نام کاربری" required>
                            <input class="login-input" type="email" placeholder="پست الکترونیکی" required>
                            <input class="login-input" type="password" placeholder="کلمه ی عبور" required>
                        </div>
                        <div class="button-div">
                            <button class="login-button" type='submit'>ثبت نام</button>
                            <label class="switch-label" for='form-switch'>وارد شویــد</label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="top">
        <nav class="navbar navbar-light bg-light">
            @auth
            <span>
                <a  href="{{url('user')}}">
                    <i class="far fa-user" style=" border: 1px solid;padding: 10px;border-radius: 100%;"></i>
                    {{Auth::user()->name}}
                </a>
                <a href="{{ url('logout') }}">
                    | خروج |
                </a>
            </span>
            @endauth

            @guest
            <a href="login">
                <i class="far fa-user" style=" border: 1px solid;padding: 10px;border-radius: 100%;"></i>عضویت/ورود
            </a>
            @endguest

            <a class="navbar-brand" href="#">
                <img alt="TiKetab-Logo" src="{{ URL::asset('assets/logo.png') }}"
                    style="top: 50%; left: 15%; position: relative;" width="90px" />
            </a>
            <div class="menuright">
                <a href={{url("/createbook")}}>
                    <i class="far fa-plus-square"></i> ثبت کتاب
                </a>
                <a href="#">
                    <i class="far fa-bell"></i> اعلان ها
                </a>
            </div>
        </nav>
    </div>

    <div class="container features">
        <div class="row">
            <div class="col-lg-4 col-md-12 m-auto left-feature">
                <p>کتاب برای فروش داری؟</p>
                <p class="p2">اینجا کلیک کن</p>
                <img src="{{ URL::asset('assets/untitled.jpg') }}" alt="">
            </div>
            <div class="col-lg-8 col-md-12 m-auto mt-lg-0 mt-md-3 right-feature">
                <p>جستجو بین کتاب ها</p>
                <div class="search">
                    <div class="name-search"><input type="text" placeholder="نویسنده یا مترجم"></div>
                    <div class="book-search"><input type="text" placeholder="نام کتاب"></div>
                </div>
                <button>🔎جستجو</button>
                <img src="{{ URL::asset('assets/untitledn.png') }}" alt="">
            </div>
        </div>
    </div>

    <div class="container book-shelves">
        <h5 class="shelve-title">کتاب ها</h5>
        <div class="row">
            @foreach ($books as $book)
            <div class="col-lg-3 book-div">
                <div>
                    <img src="{{ URL::asset('images/'.$book->image) }}" alt="">
                </div>
                <div class="book-details">
                    <div>
                        <i class="fas fa-heart like"></i>
                        <p>{{$book->name}}</p>
                        <p>{{$book->publisher}}</p>
                        <p>{{$book->price}}</p>
                        <p>{{$book->year}}</p>
                    </div>
                    <div>
                        <button><a href="book/{{$book->id}}">مشاهده</a></button>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <div class="row footer-row mt-5">
                <div class="col-lg-6 m-auto footer_item footer_item-1">
                    <h3>درباره ما</h3>
                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                        چاپگرها
                        و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز
                        و
                        کاربردهای متنوع با هدف بهبود ابزارهای
                        کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان
                        را
                        می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ
                        پیشرو در زبان فارسی ایجاد کرد.</p>
                </div>
                <div class="col-lg-4 m-auto footer_item" id="a">
                    <img src="{{ URL::asset('assets/untitled4.jpg') }}" id="b">
                    <img src="{{ URL::asset('assets/lg.png') }}" alt="" id="c">
                    <a href="#"><i id="d" class="far fa-comment"></i></a>
                    <a href="#"><i id="e" class="fas fa-envelope"></i></a>
                    <a href="#"><i id="f" class="fab fa-instagram"></i></a>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.5.0.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
</body>

</html>