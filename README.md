
<div style="direction:rtl">

## فهرست مطالب

- [فهرست مطالب](#فهرست-مطالب)
- [درباره](#درباره)
  - [راه اندازی](#راه-اندازی)
  - [نصب](#نصب)
  - [اجرا](#اجرا)

## درباره

یک سایت تمرینی با موضوع مبادله کتاب 

### راه اندازی

این پروژه از composer  , npm استفاده می کند که نیاز به نصب ان ها دارید
  
### نصب

- ایجاد فایل .env )برای مثال با کمک .env.example)
- نصب وابستگی های بک با دستور composer install
- ایجاد کلید با دستور php artisan key:generate
- اجرای دستور php artisan migrate برای ایجاد جدول ها در دیتا بیس
- اجرای دستور npm i برای نصب وابستگی های فرانت
- اجرای دستور npm run dev/prod برای کامپایل با وبپک

### اجرا

- اجرای پروژه با php artisan serve
